# YABuJoRPG

Yet Another Bullet Journal RPG
An RPG for Bullet Journaling inspired by [BuJoRPG](http://www.emeraldspecter.com/bujorpgs/) from Emerald Specter. Also takes inspiration for The Elder Scrolls, Pen and Paper RPGs and common goals systems.

### Productivity
I have recently needed to find more inspiration to not only do my journaling, but be more productive. This is a project to help with that. It should also help others in the same position.

### Adaptable
This can be adaptable to other things too, you don't need to using this in a Bullet Journal. It also allows play with other adventures from other RPG systems. It should be relatively easy to figure it out, if all else fails GM takes the reigns.

### Adventurous
Not only will you gain skills IRL, but you can use those skills in your tabletop adventures. Your attribute points you gain while completing goals will count to gameplay skill checks. Pick friends to play with, they can easily adapt this into their workflow for goals they are already working toward.


## Support
You can support the project by submitting ideas in Issues or donating to [my patreon](https://patreon.com/nephitejnf).
